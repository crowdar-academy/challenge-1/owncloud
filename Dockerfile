FROM ubuntu:22.04

ENV DEBIAN_FRONTEND noninteractive
ENV PHP_VERSION "7.4"
WORKDIR /var/www
ADD https://download.owncloud.com/server/stable/owncloud-complete-latest.tar.bz2 .

SHELL ["/bin/bash", "-o", "pipefail", "-c"]
RUN  apt-get update -y \
    	&& apt-get install software-properties-common gnupg-agent --no-install-recommends -y \  
	&& add-apt-repository ppa:ondrej/php -y \ 	
	&& apt-get update && apt-get install -y  --no-install-recommends   \
        apache2     \ 
        ca-certificates \  
        apt-utils   \ 
        libapache2-mod-php${PHP_VERSION}  \
        php${PHP_VERSION} \
        php${PHP_VERSION}-intl \
        php${PHP_VERSION}-mysql \
        php${PHP_VERSION}-mbstring \
        php${PHP_VERSION}-imagick \
        php${PHP_VERSION}-igbinary \
        php${PHP_VERSION}-gmp \
        php${PHP_VERSION}-bcmath \
        php${PHP_VERSION}-curl \
        php${PHP_VERSION}-gd \
        php${PHP_VERSION}-zip \
        php${PHP_VERSION}-imap \
        php${PHP_VERSION}-ldap \
        php${PHP_VERSION}-bz2 \
        php${PHP_VERSION}-ssh2 \
        php${PHP_VERSION}-common \
        php${PHP_VERSION}-json \
        php${PHP_VERSION}-xml \
        php${PHP_VERSION}-dev \
        php${PHP_VERSION}-apcu \
        php${PHP_VERSION}-redis \
        libsmbclient-dev \
        php-pear \
        php-phpseclib \
        libsmbclient-dev    \
        smbclient   \
        curl \
        gnupg \
        gnupg2 \
        && echo 'deb http://download.opensuse.org/repositories/isv:/ownCloud:/server:/10/Ubuntu_22.04/ /' |  tee /etc/apt/sources.list.d/isv:ownCloud:server:10.list  \
        && curl -fsSL https://download.opensuse.org/repositories/isv:ownCloud:server:10/Ubuntu_22.04/Release.key | gpg --dearmor | tee /etc/apt/trusted.gpg.d/isv_ownCloud_server_10.gpg > /dev/null \
        && apt-get update && apt-get install -y owncloud-complete-files --no-install-recommends \
        && apt-get clean \
        && rm -rf /var/lib/apt/lists/*

WORKDIR /var/www/owncloud

RUN cp -r ./* /var/www/html && chown -R www-data. /var/www/html

EXPOSE 80

ENTRYPOINT [ "/usr/sbin/apache2ctl" ]
CMD [ "-D", "FOREGROUND" ]

